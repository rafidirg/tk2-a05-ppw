from django.shortcuts import render, redirect, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from django.http import JsonResponse
from . import forms, models
from myfootprints.models import MyFootPrints

def signup(request):
    user_form = forms.CreateUserForm()
    profile_form = forms.UserProfileForm()

    if request.method == 'POST':
        user_form = forms.CreateUserForm(request.POST)
        profile_form = forms.UserProfileForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.save()
            profile = profile_form.save()
            profile.user = user
            profile.save()
            username = user_form.cleaned_data.get('username')
            messages.success(request, 'Account was created for ' + username)
            return JsonResponse({'success':'success'})
    context = { 
        'form':user_form,
        'profile_form':profile_form,
    }
    return render(request, 'authapp/signup.html', context)

def signin(request):
    if request.user.is_authenticated:
        return redirect('/')

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect(request.GET.get('next') or '/')
        else:
            messages.info(request, 'Username or Password is Incorrect')

    return render(request, 'authapp/signin.html')

def logoutuser(request):
    logout(request)
    return redirect('/auth/signin/')

@login_required(login_url='/auth/signin/')
def profile(request):
    profile_user = models.UserProfile.objects.get(user = request.user)
    total_footprints = MyFootPrints.objects.filter(user = request.user).count()
    data = {
        'profile_user' : profile_user,
        'total_footprints' : total_footprints
    }
    return render(request, 'authapp/profile.html', data)